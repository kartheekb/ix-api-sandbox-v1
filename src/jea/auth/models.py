
"""
JEA :: CRM :: Models
--------------------

"""


import secrets

from django.db import models
from django.contrib.auth.models import AbstractUser


def _make_api_key():
    """Create an api key"""
    return secrets.token_hex(8)


def _make_api_secret():
    """Create an api secret"""
    return secrets.token_urlsafe(64)


class User(AbstractUser):
    """JEA User Model"""
    # A JEA user can be associated with a customer.
    # This is the root customer for the initial api token.
    # The user is able to interact with this customer and is able to create
    # new subcustomers.
    customer = models.ForeignKey("crm.Customer",
                                 null=True,
                                 on_delete=models.CASCADE)

    api_key = models.CharField(max_length=16,
                               default=_make_api_key)

    api_secret = models.CharField(max_length=86,
                                  default=_make_api_secret)

