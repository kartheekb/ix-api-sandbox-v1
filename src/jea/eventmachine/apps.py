import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class EventmachineAppConfig(AppConfig):
    name = "jea.eventmachine"

    verbose_name = "JEA :: EVENT MACHINE"

    def ready(self):
        """Application startup"""
        logger.info("Initializing app: {}".format(self.name))

