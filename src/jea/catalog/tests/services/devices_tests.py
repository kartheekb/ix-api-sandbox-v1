
"""
Devices Service Tests
"""

import pytest
from model_bakery import baker

from jea.catalog.services import devices as devices_svc


@pytest.mark.django_db
def test_get_devices():
    """Test listing all devices"""
    device = baker.make("catalog.Device")
    devices = devices_svc.get_devices()
    assert device in devices


@pytest.mark.django_db
def test_list_devices_filter_physical_facility():
    """Test filtering device by physical facility id"""
    facility = baker.make("catalog.Facility")
    pop = baker.make("catalog.PointOfPresence", physical_facility=facility)
    device = baker.make("catalog.Device", physical_point_of_presence=pop)

    devices = devices_svc.get_devices(filters={
        "facility_id": facility.id,
    })

    assert devices.count() == 1
    assert device in devices


@pytest.mark.django_db
def test_get_device_by_id():
    """Test getting a device by it's primary key"""
    device = baker.make("catalog.Device")
    device_ = devices_svc.get_device(device=str(device.id))
    assert device.id == device_.id
