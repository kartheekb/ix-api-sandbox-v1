
"""
Test Connections Service
"""

import pytest
from model_bakery import baker
from jea.exceptions import ResourceAccessDenied

from jea.eventmachine.models import State
from jea.access.exceptions import (
    ConnectionModeUnavailable,
    DemarcationPointUnavailable,
    DemarcationPointInUse,
)
from jea.access.models import (
    Connection,
    ConnectionMode,
)
from jea.access.services import (
    connections as connections_svc,
    demarcs as demarcs_svc,
)

@pytest.mark.django_db
def test_get_connections():
    """Get a list of connections"""
    customer_a = baker.make("crm.Customer")
    customer_b = baker.make("crm.Customer")
    connection_a = baker.make(
        "access.Connection",
        scoping_customer=customer_a)
    connection_b = baker.make(
        "access.Connection",
        scoping_customer=customer_b)

    connections = connections_svc.get_connections(
        scoping_customer=customer_a)
    assert connection_a in connections
    assert not connection_b in connections

    connections = connections_svc.get_connections(
        scoping_customer=customer_b)
    assert connection_b in connections
    assert not connection_a in connections


@pytest.mark.django_db
def test_get_connection():
    """Get a single connection and check ownership"""
    customer_a = baker.make("crm.Customer")
    customer_b = baker.make("crm.Customer")
    connection_a = baker.make(
        "access.Connection",
        scoping_customer=customer_a)
    connection_b = baker.make(
        "access.Connection",
        scoping_customer=customer_b)

    connection = connections_svc.get_connection(
        connection=str(connection_a.id),
        scoping_customer=customer_a)
    assert connection == connection_a

    # Lookup with customers filtering
    # with pytest.raises(Connection.DoesNotExist):
    with pytest.raises(ResourceAccessDenied):
        connections_svc.get_connection(
            connection=str(connection_b.id),
            scoping_customer=customer_a)

    # No lookup but with ownership check
    with pytest.raises(ResourceAccessDenied):
        connections_svc.get_connection(
            connection=connection_b,
            scoping_customer=customer_a)


@pytest.mark.django_db
def test_connect_demarcation_point():
    """Test connecting a port demarc with a connection"""
    customer = baker.make(
        "crm.Customer")
    implementation_contact = baker.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    connection = baker.make(
        "access.Connection",
        scoping_customer=customer) 
    demarc = baker.make(
        "access.DemarcationPoint",
        contacts=[implementation_contact],
        scoping_customer=customer)

    connections_svc.connect_demarcation_point(
        scoping_customer=customer,
        connection=connection,
        demarcation_point=demarc)

    assert demarc in connection.demarcation_points.all()


@pytest.mark.django_db
def test_connect_demarcation_point_invalid_scoping_customer():
    """
    Test connecting a port demarc with a connection,
    but this time, the managing customers do not match.
    """
    c1 = baker.make("crm.Customer")
    c2 = baker.make("crm.Customer")
    connection = baker.make(
        "access.Connection",
        scoping_customer=c1)
    demarc = baker.make(
        "access.DemarcationPoint",
        scoping_customer=c2)

    with pytest.raises(ResourceAccessDenied):
        connections_svc.connect_demarcation_point(
            scoping_customer=connection.scoping_customer,
            connection=connection,
            demarcation_point=demarc)


@pytest.mark.django_db
def test_connect_demarcation_point_in_use():
    """
    Test connecting a port demarc with a connection,
    but this time, the port is already in use.
    """
    contact = baker.make("crm.ImplementationContact")
    connection_a = baker.make("access.Connection")
    connection_b = baker.make("access.Connection")
    demarc = baker.make(
        "access.DemarcationPoint",
        contacts=[contact],
        connection=connection_b,
        scoping_customer=connection_a.scoping_customer)

    with pytest.raises(DemarcationPointInUse):
        connections_svc.connect_demarcation_point(
            connection=connection_a,
            demarcation_point=demarc)


@pytest.mark.django_db
def test_disconnect_demarcation_point():
    """Disconnect a port demarcation point."""
    connection = baker.make(
        "access.Connection")
    demarc = baker.make(
        "access.DemarcationPoint",
        connection=connection)

    connections_svc.disconnect_demarcation_point(
        demarcation_point=demarc)

    assert not demarc.connection


@pytest.mark.django_db
def test_create_connection():
    """Create a connection without any demarcs"""
    customer = baker.make(
        "crm.Customer")

    managing_customer = baker.make(
        "crm.Customer",
        scoping_customer=customer)
    consuming_customer = baker.make(
        "crm.Customer",
        scoping_customer=customer)
    implementation_contact = baker.make(
        "crm.ImplementationContact",
        scoping_customer=customer,
        consuming_customer=consuming_customer)
    billing_contact = baker.make(
        "crm.BillingContact",
        scoping_customer=customer,
        consuming_customer=consuming_customer)

    # Create connections
    connection = connections_svc.create_connection(
        scoping_customer=customer,
        connection_input={
            "mode": ConnectionMode.MODE_STANDALONE,
            "managing_customer": managing_customer,
            "consuming_customer": consuming_customer,
            "purchase_order": "PO-23",
            "contacts": [implementation_contact, billing_contact],
        })

    assert connection


@pytest.mark.django_db
def test_assert_conneciton_can_assume_mode__blank():
    """Check extended mode validation: blank connection"""
    connection = baker.make("access.Connection")
    # This connection should be able to assume 
    # any mode, as no demarcs are assigned.
    for mode in ConnectionMode:
        connections_svc.assert_connection_can_assume_mode(
            connection, mode)

@pytest.mark.django_db
def test_assert_conneciton_can_assume_mode__blank():
    """Check extended mode validation: with demarcs"""
    connection = baker.make("access.Connection")

    # Assign two demarcs
    baker.make("access.DemarcationPoint",
        connection=connection)
    baker.make("access.DemarcationPoint",
        connection=connection)

    # This connection should not be able to assume 
    # the standalone mode.
    allowed_modes = set(ConnectionMode) - {ConnectionMode.MODE_STANDALONE}
    for mode in allowed_modes:
        connections_svc.assert_connection_can_assume_mode(
            connection, mode)

    # The standalone configuration should fail
    with pytest.raises(ConnectionModeUnavailable):
        connections_svc.assert_connection_can_assume_mode(
            connection, ConnectionMode.MODE_STANDALONE)


@pytest.mark.django_db
def test_update_connection():
    """Update the settings and properties of a connection"""
    customer = baker.make(
        "crm.Customer")
    impl_contact = baker.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    billing_contact = baker.make(
        "crm.BillingContact",
        scoping_customer=customer)
    connection = baker.make(
        "access.Connection",
        scoping_customer=customer)

    # Create connections
    connection = connections_svc.update_connection(
        scoping_customer=customer,
        connection=connection,
        connection_update={
            "mode": ConnectionMode.MODE_STANDALONE,
            "managing_customer": customer.pk,
            "consuming_customer": customer.pk,
            "purchase_order": "PO-23",
            "contacts": [impl_contact.pk, billing_contact.pk],
        })

    assert connection


@pytest.mark.django_db
def test_archive_connection():
    """Destroy a connection and release all port demarcs"""
    connection = baker.make(
        "access.Connection")
    demarc = baker.make(
        "access.DemarcationPoint",
        connection=connection)

    connections_svc.archive_connection(connection=connection)

    demarc.refresh_from_db()
    assert not demarc.connection

    connection.refresh_from_db()
    assert connection.state == State.ARCHIVED

