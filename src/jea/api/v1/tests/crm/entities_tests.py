
import pytest
from model_bakery import baker
from rest_framework import serializers
from ixapi_schema.v1.entities import crm


@pytest.mark.django_db
def test_customer_serializer():
    """Test customer serializer"""
    root_customer = baker.make("crm.Customer")
    customer = baker.make("crm.Customer", parent=root_customer)

    serializer = crm.Customer(customer)

    # Assertations
    assert serializer.data["parent"] == str(root_customer.id)


@pytest.mark.django_db
def test_polymorphic_contact_serialization__legal():
    """Test polymorphic contact serializer"""
    legal_contact = baker.make("crm.LegalContact")

    # Serialization
    serializer = crm.Contact(legal_contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__legal():
    """Test polymorphic contact deserialization"""
    customer = baker.make("crm.Customer")
    # Request data:
    data = {
        "type": "legal",
        "legal_company_name": "name",
        "address_country": "xx",
        "address_region": "region",
        "address_locality": "locality",
        "street_address": "23 streetway",
        "postal_code": "01234",
        "email": "e@mail.com",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    # Deserialization
    serializer = crm.Contact(data=data)
    assert serializer.is_valid(raise_exception=True), \
        "Serializer should accept valid data"

    # Some invalid data
    serializer = crm.Contact(data={"type": "legal", "foo": 23})
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_polymorphic_contact_serialization__noc():
    """Test polymorphic contact serializer"""
    noc_contact = baker.make("crm.NocContact")

    # Serialization
    serializer = crm.Contact(noc_contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__noc():
    """Test polymorphic contact serializer"""
    customer = baker.make("crm.Customer")
    data = {
        "type": "noc",
        "telephone": "+1234",
        "email": "ben@utzer.com",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = crm.Contact(data=data)
    assert serializer.is_valid(raise_exception=True)

    # Some invalid data
    data["email"] = "invalid.email.address"
    serializer = crm.Contact(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_polymorphic_contact_serialization__peering():
    """Test polymorphic contact serializer for peering"""
    contact = baker.make("crm.PeeringContact")

    # Serialization
    serializer = crm.Contact(contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__noc():
    """Test polymorphic contact serializer"""
    customer = baker.make("crm.Customer")
    data = {
        "type": "peering",
        "email": "ben@utzer.com",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = crm.Contact(data=data)
    assert serializer.is_valid(raise_exception=True)

    # Some invalid data
    data["email"] = "invalid.email.address"
    serializer = crm.Contact(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_polymorphic_contact_serialization__billing():
    """Test polymorphic contact serializer"""
    billing_contact = baker.make("crm.BillingContact")
    # Serialization
    serializer = crm.Contact(billing_contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__billing():
    """Test polymorphic contact serializer"""
    customer = baker.make("crm.Customer")
    data = {
        "type": "billing",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "legal_company_name": "Amazing Networks Europe Inc.",
        "address_country": "xx",
        "address_region": "region",
        "address_locality": "locality",
        "street_address": "23 Foostreet",
        "postal_code": "01234",
        "vat_number": "DE123012",
        "email": "ben@utzer.com",
    }

    serializer = crm.Contact(data=data)
    assert serializer.is_valid(raise_exception=True)



@pytest.mark.django_db
def test_polymorphic_contact_serialization__implementation():
    """Test polymorphic contact serializer"""
    implemenation_contact = baker.make("crm.ImplementationContact")

    # Serialization
    serializer = crm.Contact(implemenation_contact)
    data = serializer.data

    # Check response
    assert data, "Serializer should ouput data"


@pytest.mark.django_db
def test_polymorphic_contact_deserialization__implementation():
    """Test polymorphic contact serializer"""
    customer = baker.make("crm.Customer")
    data = {
        "type": "implementation",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "name": "Ben Utzer",
        "telephone": "+1234",
        "email": "ben@utzer.com",
    }

    serializer = crm.Contact(data=data)
    assert serializer.is_valid(raise_exception=True)

    # Some invalid data
    data["email"] = "invalid.email.address"
    serializer = crm.Contact(data=data)
    with pytest.raises(serializers.ValidationError):
        serializer.is_valid(raise_exception=True)

