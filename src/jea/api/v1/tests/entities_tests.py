
"""
Test Api Serializers
"""

from enum import Enum
from collections import namedtuple

import pytest
from model_bakery import baker
from django.db import models
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from ixapi_schema.openapi import components


#
# JeaPrimaryKeyRelatedField
#
def test_jea_primary_key_related_field():
    """Test pk to string conversion"""
    class FooModel(models.Model):
        name = models.CharField()

    field = components.PrimaryKeyRelatedField(
        queryset=FooModel.objects.all())

    foo = FooModel(pk=23, name="test")
    pk_repr = field.to_representation(foo)

    assert isinstance(pk_repr, str)
    assert pk_repr == "23"


#
# EnumField
#
def test_enum_field_to_internal_value():
    """Test enum field's to internal value"""
    class E(Enum):
        E_100 = 100
        E_1000 = 1000

    field = components.EnumField(E, values={
        E.E_100: "1e2",
    })

    value = field.to_internal_value("1e2")
    assert value == E.E_100

    value = field.to_internal_value("E_1000")
    assert value == E.E_1000

    value = field.to_internal_value("e_1000")
    assert value == E.E_1000

    value = field.to_internal_value(100)
    assert value == E.E_100

    value = field.to_internal_value(1000)
    assert value == E.E_1000

    with pytest.raises(ValidationError):
        field.to_internal_value("1e23")


def test_enum_field_to_representation():
    """Test enum field's to representation method"""
    class E(Enum):
        E_100 = 100
        E_1000 = 1000

    field = components.EnumField(E, values={
        E.E_1000: "1e3",
    })

    e_rep = field.to_representation(E.E_1000)
    assert e_rep == "1e3"

    e_rep = field.to_representation(E.E_100)
    assert e_rep == 100


