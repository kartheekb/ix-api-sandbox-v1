
"""
Test Catalog Serializers
"""

import pytest
from model_bakery import baker
from ixapi_schema.v1.entities import catalog

from jea.catalog.models import (
    PointOfPresence,
    Device, DeviceCapability,
    Facility, FacilityCluster, FacilityOperator,
    ExchangeLanNetworkProduct,
    ELineNetworkProduct,
    CloudNetworkProduct,
    ClosedUserGroupNetworkProduct,

    PRODUCT_TYPE_EXCHANGE_LAN,
    PRODUCT_TYPE_CLOSED_USER_GROUP,
    PRODUCT_TYPE_ELINE,
    PRODUCT_TYPE_CLOUD,
)

def test_facility_serializer():
    """Test facility serialization"""
    cluster = baker.prepare(FacilityCluster)
    operator = baker.prepare(FacilityOperator)
    facility = baker.prepare(Facility, cluster=cluster, operator=operator)

    serializer = catalog.Facility(facility)
    result = serializer.data
    assert result, "Serializer should have generated a result"

    assert result["cluster"] == cluster.name
    assert result["organisation_name"] == operator.name


def test_devices_serializer():
    """Test device serialization"""
    device = baker.prepare(Device)
    serializer = catalog.Device(device)

    result = serializer.data
    assert result, "The serializer should produce data"


@pytest.mark.django_db
def test_devices_serializer_capabilities():
    """Test embedded capabilities"""
    capa_1 = baker.make(DeviceCapability)
    capa_2 = baker.make(DeviceCapability)
    capa_3 = baker.make(DeviceCapability)

    device = baker.make(Device, capabilities=[capa_1, capa_2, capa_3])

    serializer = catalog.Device(device)

    result = serializer.data
    assert result, "The serializer should produce data"
    assert result["capabilities"]
    assert len(result["capabilities"]) == 3


def test_device_capability_serializer():
    """Test the serialization of a device capability"""
    capa = baker.prepare(DeviceCapability)
    serializer = catalog.DeviceCapability(capa)

    assert serializer.data, "Serializer should produce data"


@pytest.mark.django_db
def test_point_of_presence_serializer():
    """Test serialization of a pop"""
    device_a = baker.make(Device, name="device_a")
    device_b = baker.make(Device, name="device_b")

    capa = baker.make(DeviceCapability, device=device_a)

    facility = baker.make(Facility)
    remote_facility = baker.make(Facility)

    pop_a = baker.make(PointOfPresence,
                       physical_devices=[device_a],
                       reachable_devices=[device_b],
                       physical_facility=facility)

    pop_b = baker.make(PointOfPresence,
                       physical_devices=[device_b],
                       physical_facility=remote_facility)

    serializer = catalog.PointOfPresence(pop_a)
    result = serializer.data
    assert result, "Serializer should produce data without crashing."

#
# Polymorphic Product Serialization
#
def test_polymorphic_product_serialization():
    """Test polymorphic product serializer"""
    exchange_lan_product = baker.prepare(ExchangeLanNetworkProduct)
    eline_product = baker.prepare(ELineNetworkProduct)
    closed_user_group_product = baker.prepare(ClosedUserGroupNetworkProduct)
    cloud_product = baker.prepare(CloudNetworkProduct)

    # Serialize products
    result = catalog.Product(exchange_lan_product).data
    assert result["type"] == PRODUCT_TYPE_EXCHANGE_LAN

    """
    result = ProductSerializer(eline_product).data
    assert result["type"] == PRODUCT_TYPE_ELINE

    result = ProductSerializer(closed_user_group_product).data
    assert result["type"] == PRODUCT_TYPE_CLOSED_USER_GROUP

    result = ProductSerializer(cloud_product).data
    assert result["type"] == PRODUCT_TYPE_CLOUD
    """
