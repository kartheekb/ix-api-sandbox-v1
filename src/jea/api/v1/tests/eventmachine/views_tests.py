
"""
Test event views
"""

import pytest
from model_bakery import baker

from jea.api.v1.tests import authorized_requests
from jea.api.v1.eventmachine import views


@pytest.mark.django_db
def test_events_view_set__list():
    """Test getting all events"""
    customer = baker.make("crm.Customer")
    view = views.EventsViewSet.as_view({"get": "list"})
    request = authorized_requests.get(customer)
    response = view(request)

    assert response.status_code == 200
