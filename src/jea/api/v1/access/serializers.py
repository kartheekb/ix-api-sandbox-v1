
"""
Service Access Serialization
"""

from ixapi_schema.v1.entities import access

from jea.access import models as access_models


def get_config_update_serializer(config):
    """
    Get the right network service config serializer
    for a given config object.

    :param config: A network service or feature config
    """
    # Network Services
    if isinstance(config, access_models.ExchangeLanNetworkServiceConfig):
        return access.ExchangeLanNetworkServiceConfigUpdate
    if isinstance(config, access_models.ClosedUserGroupNetworkServiceConfig):
        return access.ClosedUserGroupNetworkServiceConfigUpdate
    if isinstance(config, access_models.ELineNetworkServiceConfig):
        return access.ELineNetworkServiceConfigUpdate
    if isinstance(config, access_models.CloudNetworkServiceConfig):
        return access.CloudNetworkServiceConfigUpdate

    # Network Features
    if isinstance(config, access_models.BlackholingNetworkFeatureConfig):
        return access.BlackholingNetworkFeatureConfigUpdate
    if isinstance(config, access_models.RouteServerNetworkFeatureConfig):
        return access.RouteServerNetworkFeatureConfigUpdate
    if isinstance(config, access_models.IXPRouterNetworkFeatureConfig):
        return access.IXPRouterNetworkFeatureConfigUpdate

    raise TypeError("Unsupported config type.")

