
"""
JEA IPAM

Manage ip and mac addresses.
"""

import logging

from rest_framework import request, status
from ixapi_schema.v1.entities import ipam, problems

from jea.ipam.services import (
    ip_addresses as ip_addresses_svc,
    mac_addresses as mac_addresses_svc,
)
from jea.ipam.filters import IpAddressFilter, MacAddressFilter
from jea.api.v1.permissions import require_customer
from jea.api.v1.viewsets import JEAViewSet
from jea.api.v1.response import (
    ApiSuccess,
    ApiError
)


class IpAddressViewSet(JEAViewSet):
    """
    An `IP` is a IPv4 or 6 addresses, with a given validity period.
    Some services require IP addresses to work.

    When you are joining an `exchange_lan` network service
    for example, addresses on the peering lan will be assigned
    to you.
    """
    @require_customer
    def list(self, request, customer=None):
        """
        List all ip addresses (and prefixes)
        """
        ips = ip_addresses_svc.get_ip_addresses(
            scoping_customer=customer,
            filters=request.query_params)

        return ipam.IpAddress(ips, many=True).data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """
        Get a signle ip addresses object.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_customer=customer,
            ip_address=pk)

        return ipam.IpAddress(ip_address).data

    @require_customer
    def update(self, request, customer=None, pk=None):
        """
        Update an ip address object.

        You can only update
        IP addresses within your current scope. Not all
        addresses you can read you can update.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_customer=customer,
            ip_address=pk)

        # For now only updating the fqdn is allowed
        serializer = ipam.IpAddress(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Perform update
        ip_address = ip_addresses_svc.update_ip_address(
            scoping_customer=customer,
            ip_address=ip_address,
            ip_address_update=serializer.validated_data)

        # Serialize result
        return ipam.IpAddress(ip_address).data

    @require_customer
    def partial_update(self, request, customer=None, pk=None):
        """
        Update parts of an ip address.

        As with the `PUT` opertaion, IP addresses, where you
        don't have update rights, will yield a `resource access denied`
        error when attempting an update.
        """
        ip_address = ip_addresses_svc.get_ip_address(
            scoping_customer=customer,
            ip_address=pk)

        # For now only updating the fqdn is allowed
        serializer = ipam.IpAddress(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        # Perform update
        ip_address = ip_addresses_svc.update_ip_address(
            scoping_customer=customer,
            ip_address=ip_address,
            ip_address_update=serializer.validated_data)

        # Serialize result
        return ipam.IpAddress(ip_address).data


class MacAddressViewSet(JEAViewSet):
    """
    A `MAC` is a MAC addresses with a given validity period.

    Some services require MAC addresses to work.

    The address itself can not be updated after creation.
    However: It can be expired by changing the `valid_not_before`
    and `valid_not_after` properties.
    """
    @require_customer
    def list(self, request, customer=None):
        """List all mac addresses managed by the authorized customer."""
        mac_addresses = mac_addresses_svc.get_mac_addresses(
            scoping_customer=customer,
            filters=request.query_params)

        return ipam.MacAddress(mac_addresses, many=True).data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """Get a signle mac address by id"""
        mac_address = mac_addresses_svc.get_mac_address(
            scoping_customer=customer,
            mac_address=pk)

        return ipam.MacAddress(mac_address).data

    @require_customer
    def create(self, request, customer=None):
        """Create a new mac address."""
        serializer = ipam.MacAddress(data=request.data)
        serializer.is_valid(raise_exception=True)

        # So far so good, create a fresh mac:
        mac_address = mac_addresses_svc.create_mac_address(
            scoping_customer=customer,
            mac_address_input=serializer.validated_data)

        return ApiSuccess(
            ipam.MacAddress(mac_address).data,
            status=status.HTTP_201_CREATED)

    @require_customer
    def destroy(self, request, customer=None, pk=None):
        """Remove a mac address"""
        mac_address = mac_addresses_svc.get_mac_address(
            scoping_customer=customer,
            mac_address=pk)

        mac_address = mac_addresses_svc.remove_mac_address(
            scoping_customer=customer,
            mac_address=mac_address)

        return ipam.MacAddress(mac_address).data

