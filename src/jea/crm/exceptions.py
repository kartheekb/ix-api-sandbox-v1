
from jea.exceptions import ConditionAssertionError, ValidationError

class ContactInUse(ConditionAssertionError):
    """The contact is in use"""
    default_detail = ("The contact is currently in use.")
    default_code = "contact_in_use"

class RequiredContactTypesInvalid(ValidationError):
    default_detail = ("The config requires one of each type of contact, as "
                      "described in the corresponding network-service "
                      "object.")
    default_code = "required_contact_types_invalid"
    default_field = "contacts"

