
"""
Customers Service
"""

from typing import Union, Iterable, Optional

from jea.exceptions import ResourceAccessDenied
from jea.eventmachine import active
from jea.eventmachine.models import (
    Event,
    State,
    StatusMessage,
)
from jea.crm.models import (
    Customer,
)
from jea.crm.filters import (
    CustomerFilter,
)
from jea.crm.events import (
    customer_created,
    customer_updated,
)


def get_customers(
        scoping_customer: Customer = None,
        filters=None,
    ) -> Iterable[Customer]:
    """
    Get the customers. If there is a managing customer
    context, limit the result set to the scope of the
    given customer.

    :param scoping_customer: The customer scope if present
    :param filters: A dict with filter paramters. This will be applied to
                    the subtrees queryset.
    """
    queryset = CustomerFilter(filters).qs

    if scoping_customer:
        current_customer = CustomerFilter(filters).qs \
            .filter(pk=scoping_customer.pk)
        queryset = queryset.filter(scoping_customer=scoping_customer)
        queryset |= current_customer

    return queryset.distinct()


def get_customer(
        scoping_customer: Customer = None,
        customer=None,
    ) -> Optional[Customer]:
    """
    Retrieve a customer from the repository.
    Implemented lookups:
        - customer_id

    :param customer_id: The identifier of the customer

    :raises ResourceAccessDenied: When permission checks fail
    """
    if not customer:
        return None

    # Resolve customer
    if not isinstance(customer, Customer):
        customer = Customer.objects.get(pk=customer)

    # Perform permission check
    if scoping_customer and \
        customer.pk != scoping_customer.pk and \
        customer.scoping_customer_id != scoping_customer.pk:
            raise ResourceAccessDenied(customer)

    return customer


@active.command
def create_customer(
        dispatch,
        scoping_customer: Customer = None,
        customer_input: dict = {},
    ) -> Customer:
    """
    Create a customer in the database.

    :param scoping_customer: An optional customer managing this customer.
    :param customer_input: Validated customer input data

    :return: a freshly created customer
    """
    parent = get_customer(
        customer=customer_input.get("parent"),
        scoping_customer=scoping_customer)

    # Persist customer
    customer = Customer(
        parent=parent,
        name=customer_input["name"],
        external_ref=customer_input.get("external_ref"),
        scoping_customer=scoping_customer)
    customer.save()

    # Trigger state management
    dispatch(customer_created(customer))

    # Refresh model after state transition
    customer.refresh_from_db()

    return customer


@active.command
def update_customer(
        dispatch,
        scoping_customer: Customer = None,
        customer: Customer = None,
        customer_update: dict = {},
    ) -> Customer:
    """
    Update a given customer in the database.

    :param customer_update: A validated customer update.
    :param scoping_customer: The current managing customer.
    """
    customer = get_customer(scoping_customer=scoping_customer,
                            customer=customer)

    # Set properties
    if "name" in customer_update.keys():
        customer.name = customer_update["name"]
    if "external_ref" in customer_update.keys():
        customer.external_ref = customer_update["external_ref"]
    if customer_update.get("parent"):
        parent = get_customer(
            scoping_customer=scoping_customer,
            customer=customer_update["parent"])
        customer.parent = parent

    customer.save()
    dispatch(customer_updated(customer))

    # Refresh from database after state transition
    customer.refresh_from_db()

    return customer


def delete_customer(
        scoping_customer=None,
        customer=None,
    ):
    """
    Destroy a customer.
    Might fail, if protected models are affected.

    :param scoping_customer: The customer managing the
        customer marked for deletion.
    :param customer: The customer to delete.
        Can be an id, which will be resolved.
    """
    raise NotImplementedError

