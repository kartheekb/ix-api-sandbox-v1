
"""
Admin Interface for Customers and Contacts
"""

from django.contrib.admin import (
    ModelAdmin,
    StackedInline,
)

from jea import admin
from jea.crm.models import (
    Customer,
    BillingContact,
    ImplementationContact,
    NocContact,
    LegalContact,
)

#
#Customers
#

class LegalContactInline(StackedInline):
    """Legal Contact Inline Admin"""
    model = LegalContact
    extra = 0
    fk_name = "consuming_customer"


class BillingContactInline(StackedInline):
    """Billing Contact Inline Admin"""
    model = BillingContact
    extra = 0
    fk_name = "consuming_customer"


class NocContactInline(StackedInline):
    """NOC Contact Inline Admin"""
    model = NocContact
    extra = 0
    fk_name = "consuming_customer"


class ImplementationContactInline(StackedInline):
    """Implementation Contact Inline Admin"""
    model = ImplementationContact
    extra = 0
    fk_name = "consuming_customer"


class CustomerAdmin(ModelAdmin):
    """The customer admin"""
    inlines = (
        LegalContactInline,
        BillingContactInline,
        NocContactInline,
        ImplementationContactInline,
    )


admin.site.register(Customer, CustomerAdmin)

