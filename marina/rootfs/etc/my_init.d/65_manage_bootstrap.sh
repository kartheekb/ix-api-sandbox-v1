#!/usr/bin/env bash

if [[ ${JEA_SBX_DB_RESET} -eq 1 ]]; then
/sbin/setuser app python3 /home/app/sandbox/src/manage.py bootstrap -y -n uberall-ix -a 65530 -c pilotreseller | tee /home/app/reset-logs/db-$(date +%d%m%y_%H%M%S).txt
fi
