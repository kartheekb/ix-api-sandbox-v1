#!/usr/bin/env bash

_custom_init() {
  if [[ ${JEA_SBX_NO_INIT_SCRIPTS} -eq 1 ]]; then
    /sbin/my_init --skip-startup-files -- "$@"
  elif [[ ${JEA_SBX_QUIET} -eq 1 ]]; then
    /sbin/my_init --quiet -- "$@"
  else
    /sbin/my_init -- "$@"
  fi
}

_post_init() {

  if [[ ${JEA_SBX_DEBUG} -eq 1 ]]; then
    _custom_init "$@"
  else
    _custom_init /sbin/setuser app "$@"
  fi
}

_post_init "$@"
